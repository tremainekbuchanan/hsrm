var express = require('express');
var bodyparser = require('body-parser');
var app = express();

app.use(express.static(__dirname + '/static'));
app.use(bodyparser.urlencoded({ extended : false}));
app.set('view engine', 'ejs');

app.get('/', function(req, res){
	res.render('pages/login');	
});

app.get('/dashboard', function(req, res){
	res.render('pages/index');	
});

app.get('/houses', function(req, res){
	res.render('pages/houses');
});

app.get('/students', function(req, res){
	res.render('pages/students');
});

app.post('/students', function(req, res){
	console.log('Posted');
	//res.render('pages/students');
});

app.get('/reports', function(req, res){
	res.render('pages/reports');
});

app.listen(8081);
console.log("App Started at");