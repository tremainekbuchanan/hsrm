var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var LogSchema = new Schema({
	ip_address		: String,
	user_id			: String, //system or user
	username		: String,
	created_at		: { type: Date },//time server completed request
	action			: String, //what action was take
	status_code		: String //HTTP status code
}, {collection: 'logs'});

module.exports = mongoose.model('log', LogSchema);