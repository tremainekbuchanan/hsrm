var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var testSchema = new Schema({ name			: String,
							  description	: String,
							  grade			: String,
							  letter		: String,
							  absent		: String,
							  notes			: String,
							  house_grade	: String,
							  year			: String,
							  deleted       : Boolean,
							  created_at	: { type: Date, default: Date.now() } 
							});

var StudentSchema = new Schema({
	name    	: String,
	address 	: String,
	type		: String,
	house		: String,
	quid_player	: String,
	tests		: [testSchema],
	final_grade	: String,   
	deleted     : Boolean,  
	created_at	: { type: Date, default: Date.now() }
}, {collection: 'students'});

module.exports = mongoose.model('students', StudentSchema);