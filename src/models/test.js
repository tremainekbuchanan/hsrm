var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var TestSchema = new Schema({
	name    	: String,
	description	: String,
	grade		: String,
	letter		: String,
	absent		: String,
	notes		: String,
	house_grade	: String,
	year		: String,
	created_at	: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('tests', TestSchema);