var express = require('express');
var bodyparser = require('body-parser');
var mongoose = require('mongoose');
var request_ip = require('request-ip');
var cookieParser = require('cookie-parser');
var session = require('express-session')
var Student = require('./models/student.js');
var Test = require('./models/test.js');
var Log = require('./models/log.js');
var User = require('./models/user.js');
var db = mongoose.connection;
var app = express();

app.use(express.static(__dirname + '/static'));
app.use(bodyparser());
app.use(cookieParser());
app.use(session({secret:'somesecrettokenhere', expires : new Date(Date.now() + 3600000)}));
app.set('view engine', 'ejs');

db.on('error', console.error);
db.once('open', function(){
	console.log('Database opened');
});

mongoose.connect('mongodb://localhost/test');

/*===================================
=            Login Route            =
===================================*/
/**
* GET Login Route - renders login page.
**/
app.get('/login', function(req, res){
	res.render('pages/login');	
});
/**
* GET Logout Route - logs out use and renders login page.
**/
app.get('/logout', function(req, res){
	if(req.session.user){
		req.session.destroy(function(err){
			if(err) return console.error(err);
			console.log("Session destroyed");
		});
	}else{
		console.log("Empty");
	}
	res.redirect('/login');	
});
/**
* POST Login Route - Allow users to login.
**/
app.post('/login', function(req, res){
	var uname = req.body.uname;
	var password = req.body.password;
	User.findOne({ "uname" : uname },function (err, user) {
	  if (err) return console.error(err);
	  if(user.password === password){
	  	var rights = user.privilages;
	  	req.session.user = {uid: user._id, uname: uname, rights: rights};
	  	res.redirect('/students');
	  }else{
	  		res.redirect('/login');
	   }	  
	});	
});

app.post('/user/create',function(req, res){

});
/*-----  End of Login Route  ------*/

/*=============================================
=            Students Record Routes           =
=============================================*/
/**
* GET Route - get all students from database.
**/
app.get('/students', function(req, res){
	// var rights = req.session.user.rights;
	Student.find(function (err, students) {
	  if (err) return console.error(err);
	  res.render('pages/students',{students:students});
	});
});
/**
* POST Route - create a student record and save to database.
**/
app.post('/students', function(req, res){
	var testScores = [];
	var student = new Student({
						name: req.body.fullname,
						address: req.body.address,
						type: req.body.type,
						house: req.body.house,
						quid_player : req.body.quid_player,
						tests: testScores,
						final_grade: 0
					});
	student.save(function(err){
		if(err){
			console.log(err);
		}else{
			res.redirect('/students');
		}		
	});
});
/*=============================================
=           Students Tests Route           =
=============================================*/
/**
* POST Route - create a student test record and save to database.
**/
app.post('/students/:id/tests', function(req, res){
	var id = req.body.id;
	var method = req.body._method;
	// switch(method){
	// 	case "put": //put
	// 		break;
	// 	case " " //post 
	// 		break;
	// 	default:
	// 		console.log('Nothing sent');
	// 		break;
	// }
	// var name = req.body.name;
	// var description = req.body.description;
	// var grade = req.body.grade;
	// var letter = req.body.letterGrade || 0;
	// var absent = req.body.absent;
	// var notes = req.body.notes;
	// var housePoints = req.body.housePoints || 0;
	// 	Student.findOne({ "_id" : id }, function (err, student) {
	// 		  if (err) return console.error(err);
	// 		  student.tests.push({name:name,description:description.trim(),grade:grade,letter:letter,absent:absent,notes:notes.trim(),house_grade:housePoints,year:"2015"});
	// 		  student.save(function(err){
	// 			if(err){
	// 				console.log(err);
	// 			}else{
	// 				console.log('Saved');
	// 			}		
	// 	});
	// });
	res.redirect('/profile?id='+id);
});
/**
* UPDATE Route - update a test score.
**/
app.post('/students/:id/:grade/:gid', function(req, res){
		var grade = req.body.grade;
		var student_id = req.body.id;
		var gid = req.body.gid;
		Student.findOne({"_id" : student_id}, function(err, student){
			var doc = student.tests.id(gid);
			var tests_doc = student.tests;
			doc.grade = grade;
			doc.letter = determineLetterGrade(parseInt(grade));
			student.final_grade = determineFinalGrade(tests_doc);
			doc.house_grade = determineHousePoints(grade,student.house,student.type);
			student.save(function(err){
				if(err) return console.log(err);
				res.redirect('/reports');
			});
		});		
});
/*-----  End of Students Test Route ------*/

/*-----  End of Section comment block  ------*/
app.get('/dashboard', function(req, res){
	res.render('pages/index');	
});

app.get('/houses', function(req, res){
	res.render('pages/houses');
});




app.get('/profile', function(req, res){
	// var rights = req.session.user.rights;
	var student_id = req.query.id;
	Student.findOne({ "_id" : student_id }, function (err, student) {
	  if (err) return console.error(err);
	  res.render('pages/profiles',{student: student});
	});
});

// app.post('/students', function(req, res){
// 	var testScores = [];
// 	var student = new Student({
// 						name: req.body.fullname,
// 						address: req.body.address,
// 						type: req.body.type,
// 						house: req.body.house,
// 						quid_player : req.body.quid_player,
// 						tests: testScores,
// 						final_grade: 0
// 					});
// 	student.save(function(err){
// 		if(err){
// 			console.log(err);
// 		}else{
// 			res.redirect('/students');
// 		}		
// 	});
// });



app.get('/reports', function(req, res){
	
	Student.find(function (err, students) {
	  if (err) return console.error(err);
	  var scores = [];
	  var sum = 0;
	  var final_grade = [];
	  students.forEach(function(element, index){
	  	element.tests.forEach(function(element, index){
	  		scores.push(parseInt(element.grade));
	  	});
	  	scores.sort(function(a,b){return b - a});
	  	for(i=0;i<6;i++){
	  		sum += scores[i];
	  	}
	  	final_grade.push((sum/6).toFixed(1));
	  	sum = 0;
	  	scores = [];
	  });
	  console.log(students);	  
	  res.render('pages/reports',{students:students, final_grade:final_grade});
	 });
	
});

app.listen(process.env.PORT || 8081);
console.log("App Started");

function determineLetterGrade(grade){
	var letterGrade = "Invalid";

	if(grade === 0){
		//considered deleted
		letterGrade = "Deleted";
		return letterGrade;
	}

	if(grade >= 90 && grade <= 100){
		letterGrade = "A+";
	}else if(grade >= 80 && grade < 90){
		letterGrade = "A";
	}else if(grade >=70 && grade < 80){
		letterGrade = "B";
	}else if(grade >= 60 && grade < 70){
		letterGrade = "C";
	}else if(grade >=50 && grade < 60){
		letterGrade = "D";
	}else if(grade > 0 && grade < 50){
		letterGrade = "F";
	}

	return letterGrade;
	//console.log(letterGrade);
}

function determineFinalGrade(doc){
	var sum = 0;
	for(var i=0;i<6;i++){
		sum += parseInt(doc[i].grade);
	}

	return (sum / 6).toFixed(1);
	//return 90;
}

function determineHousePoints(grade,house,type){
	var points = 0;	
	if(house === "Salazar Slytherin" && grade >= 80){
			if(type === "Muggle"){
				points = 100;
			}
	}else if(grade >= 90 && type != "Muggle"){
		points = 50;
	}
	return points;		
	
}