//console.log("Fired");
$('#myModal').on('show.bs.modal', function(e) {
    var stuId = $(e.relatedTarget).data('id');
    var gid = $(e.relatedTarget).data('gid');
    //console.log(gid);
    $(e.currentTarget).find("input[id=student_id]").val(stuId);
    $(e.currentTarget).find("input[id=gid]").val(gid);

});

$('#edit_test').on('show.bs.modal', function(e) {
    //console.log("Yes");
    var stuId = $(e.relatedTarget).data('id');
    var gid = $(e.relatedTarget).data('gid');
    // $(e.currentTarget).find("input[id=id]").val(id);
    // $(e.currentTarget).find("#year option:selected").text("Test");
    // $(e.currentTarget).find("#name option:selected").text("Test");
    // $(e.currentTarget).find("#description").text("Test");
    // $(e.currentTarget).find("#grade").val("Test");
    // $(e.currentTarget).find("#letterGrade").text("Test");
    // $(e.currentTarget).find("#absent option:selected").text("Test");
    // $(e.currentTarget).find("#notes").text("Test");
    // $(e.currentTarget).find("#houseGrade").text("Test");
    // $(e.currentTarget).find("#grade").focusout(function(){
    //     var grade = $("#grade").val();
    //     var letterGrade = determineLetterGrade(parseInt(grade));
    //     var houseGrade = determineHousePoints("Salazar Slytherin", grade, "Muggle");
    //     $("#letterGrade").text(letterGrade);
    //     $(e.currentTarget).find("input[id=letterGrade]").val(letterGrade);
    //     $(e.currentTarget).find("input[id=_method]").val("put");
    //     $("#houseGrade").text(houseGrade);
    //     $(e.currentTarget).find("input[id=housePoints]").val(houseGrade);
    // });
    $(e.currentTarget).find("input[id=id]").val(stuId);
    $(e.currentTarget).find("input[id=_method]").val("put");
    //console.log(val);
});

$('#test_input').on('show.bs.modal', function(e) {
    var house = $(e.relatedTarget).data('house');
    var type = $(e.relatedTarget).data('type');
    var id = $(e.relatedTarget).data('id');
    $(e.currentTarget).find("input[id=id]").val(id);
    $(e.currentTarget).find("#grade").focusout(function(){
    	console.log("House: " + house + " " + "Type:" + type);
    	var grade = $("#grade").val();
    	var letterGrade = determineLetterGrade(grade);
    	var houseGrade = determineHousePoints(house, grade, type);
    	$("#letterGrade").text(letterGrade);
    	$(e.currentTarget).find("input[id=letterGrade]").val(letterGrade);
    	$("#houseGrade").text(houseGrade);
    	$(e.currentTarget).find("input[id=housePoints]").val(houseGrade);
    });
   
});

function determineLetterGrade(grade){
	var letterGrade = "Invalid";
	if(grade >= 90 && grade <= 100){
		letterGrade = "A+";
	}else if(grade >= 80 && grade < 90){
		letterGrade = "A";
	}else if(grade >=70 && grade < 80){
		letterGrade = "B";
	}else if(grade >= 60 && grade < 70){
		letterGrade = "C";
	}else if(grade >=50 && grade < 60){
		letterGrade = "D";
	}else if(grade > 0 && grade < 50){
		letterGrade = "F";
	}	
	return letterGrade;
	//console.log(letterGrade);
}

function determineHousePoints(house,grade,type){
	var points = 0;	
	if(house === "Salazar Slytherin" && grade >= 80){
			if(type === "Muggle"){
				points = 100;
			}
	}else if(grade >= 90 && type != "Muggle"){
		points = 50;
	}
	return points;
	//return 100;		
	
}